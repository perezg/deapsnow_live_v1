# Deapsnow live operational models

This repository contains code that enables the operation of two avalanche
danger prediction models that are used for predictng 
avalache danger levels in Switzerland.

The code contains several major components:
- random forests (RFs), for dry avalnches, located at `deapsnow_live/models/plain_cnn_1D`


In addition to the code for all of the modles mentioned above. There are
are modules in the code that are shared between all or several models.
These are as follows:
- `deapsnow_live/parsing`: used to download snowpack data and parse it into files that
the models in this repo can then use
- `deapsnow_live/postprocessing`: used to process and summarize the output produced by the models
- `deapsnow_live/profiles`: used to parse and pre-process snow profile
- `deapsnow_live/utils`: common utilities - currently contains only a single function to send emails
with model results

## How to run

All "entrypoints" into running the models are described in the `Makefile`. During regular
live operation, a crontab is setup that invokes the following commands in order:

```bash
make download_snowpack_data
make input_files
make predict

```

The environment used to run the models is specified in the `environment.yml` file.
This can be used to recreate a similar environment elsewhere by simply doing:

```
conda env create -f environment.yml
```


## Current live operation setup

Currently we use a cronjob to run the models every 3 hours at 55 minutes past the hour.
This gives enough time that in most cases we get complete set of results from the snowpack
simulations that are used as inputs.

The cronjob is setup with the following chain of commands:
```bash
RUNTIME=$(date +\%Y-\%m-\%d\ \%H:00:00) LOG="logs/$(date +\%Y-\%m-\%d\T\%H:\%M).txt"  \
bash -c 'cd /home/deapsnow/deapsnow/deapsnow_live_revamp && source /home/deapsnow/miniconda3/bin/activate torch && RUNTIME="$RUNTIME" make download_snowpack_data &>> $LOG && RUNTIME="$RUNTIME" make input_files &>> $LOG && RUNTIME="$RUNTIME" make predict &>> $LOG 
```

The schedule for this is `55 */3 * * *`

The above command saves all the outputs and errors in log files named with the time the prediction ran.
In addition it also activates the correct anaconda environment.

There is an additional cronjob that runs to clean up old logs periodically.
This is as follows:
```
0 1 */7 * * find /home/deapsnow/deapsnow/deapsnow_live_revamp/logs -type f -mtime +14 -delete
```

The logs located in the `logs` folder at the root of this repository are invaluable
when it comes to troubleshooting and/or understanding problems with the models and the operation.

## Data files and location
Currently there are two classes of data consumed/produced by the models:
1. Static data that does not change as the models run from one day to another
2. Dynamic data that changes with every time that the models run

**NOTE**: any data files regardless of whether they are binary or text files that 
are checked into the repository should be committed with Git LFS. Failure to do so
will make the repository practically unusable. If a large file is committed and
pushed to the repository outside of LFS then one needs to remove this file from 
**every** committ in the whole history in the repository. In addition to this
any local clone of the repository will have to pull this new, re-written history as
well. 

### Static data
This data is located mainly in the `data` folder at the root of this repository.
It includes things like:
- trained model parameter files
- some sample data
- some data that is used to train the models
- data about the different danger level regions

### Dynamic data
This includes the majority of the data that the models consume and produce.
This data depends on the time period for which the models run - therefore
all of these files will contain some sort of a timestamp in their filename.
Because the size of this data is larger than the static data it is not stored
in the repository.

The location for the dynamic data can be found at `../live-data-revamp/`
relative to the root of this repository. The location for this folder can
be controlled and adjusted by using the `DEAPSNOW_PATH_PREFIX` environment
variable. By default and during live operation this is set to `.`. So the 
file paths for every dynamic input and output the model produces are determined
as `$DEAPSNOW_PATH_PREFIX/../live-data-revamp/`. Since the `Makefile` and all
commands to run the models are invoked from the root of the repository
then the paths for saving all files are all relative to the root of this
repository. However if desired one can modify the path with the environment
variable so that the paths are absolute. All the models will create the necessary
folders where things are saved if the folders defined in these paths do not exist.
The folder structure does not need to be created manually before the models run.

The `../live-data-revamp` folder has the following contents:
- `snowpack-data-nowcast/`: The latest `.smet` and `.pro` generated by the snowpack model nowcast. 
The `make download_snowpack_data` downloads new data here every 3 hours.
- `snowpack-data-forecast/`: The latest `.smet` and `.pro` generated by the snowpack model forecast. 
The `make download_snowpack_data` downloads new data here every 3 hours.
- `actual_forecasts/`: This contains the most up-to-date human-made avalanche danger predictions, 
in other words this is the "ground truth" for the models here
- `rf_model_data/`: Data files derived from snowpack model output that are then used as inputs in the RF and CNN models.

- `predictions/`: The predictions made by all the models in this repo. This is organized first
by the different models (i.e. CNNs, RFs, etc.) and then by whether the models ran a forecast or a nowcast.

NOTE: If not specified, the time when the model has run (i.e. `$RUNTIME`) is used in the file names. So
the file located at `../live-data-revamp/predictions/rf_models/nowcast/2022-02-09_03_nowcast_type1_all_aspects.csv`
is the nowcast that ran at 3AM on February 9, for the random forests model. The additional parts of the file
name indicate what feature set the model used and whether it ran for a single aspect (which is usually flat)
or for all aspects (flat, north, east, south, west). 

### Important 

This repository includes a machine learning model trained and tested with data from Switzerland. While this model has been trained and validated within the context of the provided dataset, please note the following important considerations:

- Model Transferability: The model’s predictions may not be directly transferable to locations outside of the original dataset’s scope. If you intend to apply this model to new locations or under different conditions, additional validation is necessary to ensure accuracy and reliability.

- Limitations in Operational Use: This model should not be used for operational avalanche forecasting without validating its predictions and understanding the limitations of the model. The predictions are intended for research and exploratory purposes only. 


## REFERENCES: 

The description of the performance of one of the Random Forest models ('RF_tidy_1') used to predict avalanche danger levels for dry-snow conditions in Switzerland during the initial live-testing in the winter season of 2020–2021:

- Pérez-Guillén, C., Techel, F., Volpi, M., and van Herwijnen, A.: Assessing the performance and explainability of an avalanche danger forecast model, EGUsphere, https://doi.org/10.5194/egusphere-2024-2374, 2024.

The developement of the models is described in:

- Pérez-Guillén, C., Techel, F., Hendrick, M., Volpi, M., van Herwijnen, A., Olevski, T., Obozinski, G., Pérez-Cruz, F., and Schweizer, J.: Data-driven automated predictions of the avalanche danger level for dry-snow conditions in Switzerland, Nat. Hazards Earth Syst. Sci., 22, 2031–2056, https://doi.org/10.5194/nhess-22-2031-2022, 2022.

## Cite this work: 


    @Article{nhess-22-2031-2022,
    AUTHOR = {P\'erez-Guill\'en, C. and Techel, F. and Hendrick, M. and Volpi, M. and van Herwijnen, A. and Olevski, T. and Obozinski, G. and P\'erez-Cruz, F. and Schweizer, J.},
    TITLE = {Data-driven automated predictions of the avalanche danger level for dry-snow conditions in Switzerland},
    JOURNAL = {Natural Hazards and Earth System Sciences},
    VOLUME = {22},
    YEAR = {2022},
    NUMBER = {6},
    PAGES = {2031--2056},
    URL = {https://nhess.copernicus.org/articles/22/2031/2022/},
    DOI = {10.5194/nhess-22-2031-2022}
    }
    
    @Article{egusphere-2024-2374,
    AUTHOR = {P\'erez-Guill\'en, C. and Techel, F. and Volpi, M. and van Herwijnen, A.},
    TITLE = {Assessing the performance and explainability of an avalanche danger forecast model},
    JOURNAL = {EGUsphere},
    VOLUME = {2024},
    YEAR = {2024},
    PAGES = {1--29},
    URL = {https://egusphere.copernicus.org/preprints/2024/egusphere-2024-2374/},
    DOI = {10.5194/egusphere-2024-2374}
    }


## Contact

Cristina Pérez Guillén: cristina.perez@slf.ch