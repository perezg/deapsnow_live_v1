from deapsnow_live.models.rf_models.flat import dl_predictions
from deapsnow_live.config import get_args_parser
from deapsnow_live.models.rf_models.config.features import set1


def main(date, forecast):
    model = "RF_tidy_1.sav"
    dl_predictions(
        "nowcast", date, model, "type1", set1, "yes_tidy"
    )

    
    model = "RF_all_1.sav"
    dl_predictions(
        "nowcast", date, model, "type1", set1, "yes_all"
    )

    
    if forecast:
        model = "RF_tidy_1.sav"
        dl_predictions(
            "forecast", date, model, "type1", set1, "yes_tidy"
        )

        

        model = "RF_all_1.sav"
        dl_predictions(
            "forecast", date, model, "type1", set1, "yes_all"
        )

        


if __name__ == "__main__":
    parser = get_args_parser()
    args = parser.parse_args()
    main(args.date, args.forecast)
