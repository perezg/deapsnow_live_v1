import pandas as pd
from sklearn.ensemble import RandomForestClassifier

from deapsnow_live.models.rf_models.utils import train_test
from deapsnow_live.models.rf_models.config.features import set1

if __name__ == "__main__":
    # Read the data
    dry_all = pd.read_csv("data/data_all/Dry_flat_all_mean.csv").drop("Unnamed: 0", 1)
    dry_tidy = pd.read_csv("data/data_all/Dry_flat_tidy_mean.csv").drop("Unnamed: 0", 1)

    rf1_all = RandomForestClassifier(
        class_weight="balanced",
        max_depth=50,
        max_features="log2",
        min_samples_leaf=6,
        n_jobs=-1,
        min_samples_split=12,
        n_estimators=1000,
        random_state=42,
    )
    train_test(
        dry_all, set1, rf1_all, "data/models/rf_models/RF_all_1.sav", "All"
    )

    

    rf1_tidy = RandomForestClassifier(
        class_weight="balanced",
        max_depth=40,
        max_features="log2",
        min_samples_leaf=5,
        n_jobs=-1,
        min_samples_split=10,
        n_estimators=1000,
        random_state=42,
    )
    train_test(
        dry_tidy,
        set1,
        rf1_tidy,
        "data/models/rf_models/RF_tidy_1.sav",
        "Tidy",
    )

    
