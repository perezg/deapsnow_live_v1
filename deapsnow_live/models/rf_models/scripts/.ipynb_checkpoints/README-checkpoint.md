# Programs to read profile data (.pro & ext.pro) from the server and extract features:

    dry_feat_extract_opt.py
    features_profiles_script_path.py
    hhi_reparam.py
    layer_lab_v3.py
    level_0_profile_extract_dry.py
    read_profile.py
    read_profile_bin.py
    stab_feat.py
    weakest_pwl.py


---------------------------------------------------------------------------------------------------------------

# Programs to read smet data (.smet & ext.smet) from the server, resample and compute three new features:

    Nowcast data: resample_smet_server_nowcast.py  (SNOWPACK simulations with 3h resolution)
    Forecast data: resample_smet_server_forecast.py (SNOWPACK simulations with 3h resolution)
     
    
---------------------------------------------------------------------------------------------------------------

# Program to extract features to run Random Forest:

    Nowcast data : input_data_nowcast.py
    Forecast data : input_data_forecast.py
    
### SNOWPACK time resolution

The data used to train the models and in the live test contains SNOWPACK simulations with a 3-hour resolution. In case the SNOWPACK data has a one-hour resolution, use the scripts located at: scritps/scripts_snowpack_1h. 