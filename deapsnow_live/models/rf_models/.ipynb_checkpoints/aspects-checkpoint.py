import pickle
import pandas as pd
import numpy as np
import logging
from datetime import datetime
from pathlib import Path

from deapsnow_live.models.rf_models.config.shared import path_lib
from deapsnow_live.config import date_time_format


def dl_predictions_aspects(type_pred, datetime_str, model, features, save):
    """
    type_pred = 'nowcast' or 'forecast'
    datetime_str = Prediction date ('2020-12-08 09:00:00').
                   It is validto (last date) for nowcast and
                   validfrom (first date) for forecast
    model = select model to run ('RF_tidy_1.sav')
    features = set of features. Model 'type1' only run with set_1
    save = 'yes_tidy' or 'yes_all', save the predictions in the output path
    """
    model = pickle.load(open(path_lib["models"] + "/" + model, "rb"))
    runtime = datetime.strptime(datetime_str, date_time_format)
    runtime_fin_string = runtime.strftime("%Y-%m-%d_%H")
    cols = [
        "station",
        "validfrom",
        "validto",
        "DL_f",
        "DL_n",
        "DL_e",
        "DL_s",
        "DL_w",
        "P1_f",
        "P1_n",
        "P1_e",
        "P1_s",
        "P1_w",
        "P2_f",
        "P2_n",
        "P2_e",
        "P2_s",
        "P2_w",
        "P3_f",
        "P3_n",
        "P3_e",
        "P3_s",
        "P3_w",
        "P4_f",
        "P4_n",
        "P4_e",
        "P4_s",
        "P4_w",
    ]
    df = pd.DataFrame(columns=cols)
    aspects = ["north", "east", "south", "west"]

    if type_pred == "nowcast":
        in_f = pd.read_csv(
            path_lib["input_nowcast"] + "/" + runtime_fin_string + ".csv",
            parse_dates=["validfrom", "validto"],
        ).drop(["Unnamed: 0"], axis=1)
        for col in ["station", "validfrom", "validto"]:
            df.loc[:, col] = in_f.loc[:, col]
        Xpred = in_f[features].dropna()  # Drop Nan input values
        if Xpred.shape[0] == 0:
            logging.warning(
                f"No input data for RF model with {type_pred}, {datetime_str}."
            )
            return None
        Ypred = model.predict(Xpred)
        Prob = model.predict_proba(Xpred)
        df.loc[Xpred.index, "DL_f"] = Ypred
        df.loc[Xpred.index, "P1_f"] = Prob[:, 0]
        df.loc[Xpred.index, "P2_f"] = Prob[:, 1]
        df.loc[Xpred.index, "P3_f"] = Prob[:, 2]
        df.loc[Xpred.index, "P4_f"] = Prob[:, 3]

        for aspect in aspects:
            in_aspect = pd.read_csv(
                path_lib["input_nowcast"] + "/" + runtime_fin_string + "_" + aspect + ".csv",
                parse_dates=["validfrom", "validto"],
            ).drop(["Unnamed: 0"], axis=1)
            Xpred = in_aspect[features].dropna()  # Drop Nan input values
            Ypred = model.predict(Xpred)
            Prob = model.predict_proba(Xpred)
            df.loc[Xpred.index, "DL_" + aspect[0:1]] = Ypred
            df.loc[Xpred.index, "P1_" + aspect[0:1]] = Prob[:, 0]
            df.loc[Xpred.index, "P2_" + aspect[0:1]] = Prob[:, 1]
            df.loc[Xpred.index, "P3_" + aspect[0:1]] = Prob[:, 2]
            df.loc[Xpred.index, "P4_" + aspect[0:1]] = Prob[:, 3]

    elif type_pred == "forecast":
        in_f = pd.read_csv(
            path_lib["input_forecast"] + "/" + runtime_fin_string + ".csv",
            parse_dates=["validfrom", "validto"],
        ).drop(["Unnamed: 0"], axis=1)
        for col in ["station", "validfrom", "validto"]:
            df.loc[:, col] = in_f.loc[:, col]
        Xpred = in_f[features].dropna()  # Drop Nan input values
        if Xpred.shape[0] == 0:
            logging.warning(
                f"No input data for RF model with {type_pred}, {datetime_str}."
            )
            return None
        Ypred = model.predict(Xpred)
        Prob = model.predict_proba(Xpred)
        df.loc[Xpred.index, "DL_f"] = Ypred
        df.loc[Xpred.index, "P1_f"] = Prob[:, 0]
        df.loc[Xpred.index, "P2_f"] = Prob[:, 1]
        df.loc[Xpred.index, "P3_f"] = Prob[:, 2]
        df.loc[Xpred.index, "P4_f"] = Prob[:, 3]

        for aspect in aspects:
            in_aspect = pd.read_csv(
                path_lib["input_forecast"] + "/" + runtime_fin_string + "_" + aspect + ".csv",
                parse_dates=["validfrom", "validto"],
            ).drop(["Unnamed: 0"], axis=1)
            Xpred = in_aspect[features].dropna()  # Drop Nan input values
            Ypred = model.predict(Xpred)
            Prob = model.predict_proba(Xpred)
            df.loc[Xpred.index, "DL_" + aspect[0:1]] = Ypred
            df.loc[Xpred.index, "P1_" + aspect[0:1]] = Prob[:, 0]
            df.loc[Xpred.index, "P2_" + aspect[0:1]] = Prob[:, 1]
            df.loc[Xpred.index, "P3_" + aspect[0:1]] = Prob[:, 2]
            df.loc[Xpred.index, "P4_" + aspect[0:1]] = Prob[:, 3]

    # Snow depth filter to predrict danger level:
    index = in_f.loc[(in_f["HS_mod_mean"] < 30), :].index
    df.loc[index, cols[3:]] = np.NaN

    if save.startswith("yes"):
        p = Path(path_lib["output_nowcast"])
        if not p.exists():
            logging.warning(
                f"Location for model predictions {path_lib['output_nowcast']} does not exst - "
                "creating it now..."
            )
            p.mkdir(parents=True, exist_ok=True)
        p = Path(path_lib["output_forecast"])
        if not p.exists():
            logging.warning(
                f"Location for model predictions {path_lib['output_forecast']} does not exst - "
                "creating it now..."
            )
            p.mkdir(parents=True, exist_ok=True)

    model_type = "type1"  # Only possible model for aspects
    if save == "yes_tidy":
        if type_pred == "nowcast":
            df.to_csv(
                path_lib["output_nowcast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_nowcast_"
                + model_type
                + "_tidy"
                + "_aspects"
                + ".csv"
            )
        elif type_pred == "forecast":
            df.to_csv(
                path_lib["output_forecast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_forecast_"
                + model_type
                + "_tidy"
                + "_aspects"
                + ".csv"
            )
    
    
    elif save == "yes_all":
        if type_pred == "nowcast":
            df.to_csv(
                path_lib["output_nowcast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_nowcast_"
                + model_type
                + "_all"
                + "_aspects"
                + ".csv"
            )
        elif type_pred == "forecast":
            df.to_csv(
                path_lib["output_forecast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_forecast_"
                + model_type
                + "_all"
                + "_aspects"
                + ".csv"
            )
    else:
        pass
    return df
