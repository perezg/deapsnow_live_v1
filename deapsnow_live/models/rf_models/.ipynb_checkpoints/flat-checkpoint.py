import pickle
import pandas as pd
import numpy as np
import logging
from datetime import datetime
from pathlib import Path

from deapsnow_live.models.rf_models.config.shared import path_lib
from deapsnow_live import config


def dl_predictions(type_pred, datetime_str, model, model_type, features, save):
    """
    type_pred = 'nowcast' or 'forecast'
    datetime_str = Prediction date ('2020-12-08 09:00:00'). It is validto (last date) for nowcast and
                    validfrom (first date) for forecast
    model = select model to run ('RF_tidy_1.sav')
    model_type = 'type1' (Filter snow depth < 30 cm & 28 features) 
    features = set of features. Model 'type1' only run with set_1 features and model 'type2' with set_2
    save = 'yes_tidy' or 'yes_all', save the predictions in the output path
    """
    # Pick the model:
    model = pickle.load(open(path_lib["models"] + "/" + model, "rb"))
    runtime = datetime.strptime(datetime_str, config.date_time_format)
    runtime_fin_string = runtime.strftime("%Y-%m-%d_%H")
    cols = [
        "station",
        "validfrom",
        "validto",
        "DL_prediction",
        "P_level_1",
        "P_level_2",
        "P_level_3",
        "P_level_4",
    ]
    df = pd.DataFrame(columns=cols)
    if type_pred == "nowcast":
        input_data = pd.read_csv(
            path_lib["input_nowcast"] + "/" + runtime_fin_string + ".csv",
            parse_dates=["validfrom", "validto"],
        )
    elif type_pred == "forecast":
        input_data = pd.read_csv(
            path_lib["input_forecast"] + "/" + runtime_fin_string + ".csv",
            parse_dates=["validfrom", "validto"],
        )

    for col in ["station", "validfrom", "validto"]:
        df.loc[:, col] = input_data.loc[:, col]
    # Danger level predictions
    Xpred = input_data[features].dropna()  # Drop Nan input values
    if Xpred.shape[0] == 0:
        logging.warning(
            f"No input data for RF model with {type_pred}, "
            f"{datetime_str}, {model_type}."
        )
        return None
    Ypred = model.predict(Xpred)
    Prob = model.predict_proba(Xpred)
    df.loc[Xpred.index, "DL_prediction"] = Ypred
    df.loc[Xpred.index, "P_level_1"] = Prob[:, 0]
    df.loc[Xpred.index, "P_level_2"] = Prob[:, 1]
    df.loc[Xpred.index, "P_level_3"] = Prob[:, 2]
    df.loc[Xpred.index, "P_level_4"] = Prob[:, 3]

    # Snow depth filter to predrict danger level:
    if model_type == "type1":  # Filter for snow depths > 30cm
        index = input_data.loc[(input_data["HS_mod_mean"] < 30), :].index
        df.loc[index, cols[3:]] = np.NaN
    elif model_type == "type2":  # Filter for snow depths > 100cm
        index = input_data.loc[(input_data["HS_mod_mean"] < 100), :].index
        df.loc[index, cols[3:]] = np.NaN

    if save.startswith("yes"):
        p = Path(path_lib["output_nowcast"])
        if not p.exists():
            logging.warning(
                f"Location for model predictions {path_lib['output_nowcast']} does not exst - "
                "creating it now..."
            )
            p.mkdir(parents=True, exist_ok=True)
        p = Path(path_lib["output_forecast"])
        if not p.exists():
            logging.warning(
                f"Location for model predictions {path_lib['output_forecast']} does not exst - "
                "creating it now..."
            )
            p.mkdir(parents=True, exist_ok=True)
    if save == "yes_tidy":
        if type_pred == "nowcast":
            df.to_csv(
                path_lib["output_nowcast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_nowcast_"
                + model_type
                + "_flat"
                + "_tidy"
                + ".csv"
            )
        elif type_pred == "forecast":
            df.to_csv(
                path_lib["output_forecast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_forecast_"
                + model_type
                + "_flat"
                + "_tidy"
                + ".csv"
            )
    elif save == "yes_all":
        if type_pred == "nowcast":
            df.to_csv(
                path_lib["output_nowcast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_nowcast_"
                + model_type
                + "_flat"
                + "_all"
                + ".csv"
            )
        elif type_pred == "forecast":
            df.to_csv(
                path_lib["output_forecast"]
                + "/"
                + datetime_str[0:10]
                + "_"
                + datetime_str[11:13]
                + "_forecast_"
                + model_type
                + "_flat"
                + "_all"
                + ".csv"
            )
    
    else:
        pass
    return df
