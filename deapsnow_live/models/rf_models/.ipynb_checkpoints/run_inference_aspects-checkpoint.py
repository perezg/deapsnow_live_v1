from deapsnow_live.models.rf_models.aspects import dl_predictions_aspects
from deapsnow_live.config import get_args_parser
from deapsnow_live.models.rf_models.config.features import set1


def main(date, forecast):
    model = "RF_tidy_1.sav"
    dl_predictions_aspects(
        "nowcast", date, model, set1, "yes_tidy"
    )
    
    model = "RF_all_1.sav"
    dl_predictions_aspects(
        "nowcast", date, model, set1, "yes_all"
    )
    
    
    if forecast:
        model = "RF_tidy_1.sav"
        dl_predictions_aspects(
            "forecast", date, model, set1, "yes_tidy"
        )
        
        model = "RF_all_1.sav"
        dl_predictions_aspects(
            "forecast", date, model, set1, "yes_all"
        )
        
        


if __name__ == "__main__":
    parser = get_args_parser()
    args = parser.parse_args()
    main(args.date, args.forecast)
