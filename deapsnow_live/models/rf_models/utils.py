import matplotlib.pyplot as plt
import numpy as np
import matplotlib
import pickle
from sklearn.metrics import classification_report
from pandas.plotting import register_matplotlib_converters


def plot_class_distribution(data, title, fout):
    register_matplotlib_converters()
    title_font = {
        "fontname": "Arial",
        "size": "20",
        "color": "black",
        "weight": "normal",
        "verticalalignment": "bottom",
    }
    title2_font = {
        "fontname": "Arial",
        "size": "17",
        "color": "black",
        "weight": "normal",
        "verticalalignment": "bottom",
    }
    plt.figure(figsize=(20, 7))
    dl_un, dl_counts = np.unique(data["dangerLevel"].dropna(), return_counts=True)
    _tot = np.sum(dl_counts)
    params = {"axes.titlesize": "18", "xtick.labelsize": "18", "ytick.labelsize": "18"}
    matplotlib.rcParams.update(params)
    bar = plt.bar(range(1, 5, 1), dl_counts)
    plt.xlim([0.5, 4.5])
    for rect in bar:
        height = rect.get_height()
        plt.text(
            rect.get_x() + rect.get_width() / 2.0,
            height,
            f"{np.round(height/_tot*100, decimals=2)}%",
            ha="center",
            va="bottom",
            **title2_font,
        )
    plt.title(title, **title_font)
    plt.savefig(fout)


def df_train_test(data, features, subset):
    full_df = data.copy()
    full_df.loc[
        full_df["dangerLevel"] == 5, "dangerLevel"
    ] = 4  ## Merge danger level 4 & 5
    ## Filter danger level 4
    full_df.drop(
        full_df.loc[
            (full_df["HN72_24_mean"] < 30) & (full_df["dangerLevel"] == 4),
            "HN72_24_mean",
        ].index,
        inplace=True,
    )
    if "TS2_mean" in features:
        full_df.drop(full_df.loc[(full_df["HS_mod_mean"] < 100), :].index, inplace=True)
        print("TS2 selected")
    else:
        full_df.drop(full_df.loc[(full_df["HS_mod_mean"] < 30), :].index, inplace=True)
        print("TS2 no select")
    full_df = full_df[features + ["datum", "station_code", "dangerLevel", "set"]]
    inds = full_df.notna().sum() < 0.8 * full_df["dangerLevel"].notna().sum()
    print(
        f"dropping columns because not dense enough: {full_df.loc[:,inds].columns.tolist()}"
    )
    full_df = full_df.drop(full_df.loc[:, inds].columns, axis=1)
    full_df = full_df.dropna()  ### Drop NaN
    full_df = full_df.reset_index()
    full_df = full_df.drop(columns=["index"])
    if subset == "Tidy":
        train = full_df[(full_df["set"] == "train")]
        test = full_df[(full_df["set"] == "test")]
    elif subset == "All":
        train = full_df[(full_df["set"] == "train") | (full_df["set"] == "validation")]
        test = full_df[(full_df["set"] == "test")]
    return train, test


def train_test(data, features, model, model_save_fout, subset="All"):
    bold_ = '\033[1m'
    end_ = '\033[0m'
    train, test = df_train_test(data, features, subset)
    Xtrn = train[features]
    Ytrn = train['dangerLevel']
    Xtest = test[features]
    Ytest = test['dangerLevel']
    model_fitted = model.fit(Xtrn, Ytrn)
    Ytr_hat = model_fitted.predict(Xtrn)
    Ytest_hat = model_fitted.predict(Xtest)
    report_test = classification_report(Ytest, Ytest_hat)
    print(f'{bold_}### TEST{end_}')
    print(report_test)
    report_tr = classification_report(Ytrn, Ytr_hat)
    print(f'{bold_}### TRN{end_}')
    print(report_tr)
    print(list(Xtrn), len(Xtrn.columns), len(Xtrn))
    ### Save the model:
    pickle.dump(model_fitted, open(model_save_fout, 'wb'))
    return model_fitted
