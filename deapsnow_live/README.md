# deapsnow_live_v1

This is the data repository used to run the random forest classifiers for predicting danger levels for dry-snow conditions in Switzerland.

## Information

The data used to train the models and in the live test contains SNOWPACK simulations with a 3-hour resolution. In case the SNOWPACK data has a one-hour resolution, use the scripts located at: /deapsnow_live_v1/deapsnow_live/models/rf_models/scripts/scripts_snowpack_1h. 

To run the models, it is important to keep the feature order (set1) the same as during training.  

set1 =  ["HN24_mean",
        "HN24_mean_7d",
        "HN72_24_mean",
        "HS_mod_mean",
        "ILWR_mean",
        "ISWR_dir_mean",
        "LWR_net_mean",
        "MS_Snow_mean",
        "Qg0_mean",
        "Qs_mean",
        "Qw_mean",
        "RH_mean",
        "S4_mean",
        "Sn_mean",
        "Ss_mean",
        "TSS_mod_mean",
        "TA_mean",
        "VW_drift_mean",
        "VW_mean",
        "pAlbedo_mean",
        "wind_trans24_mean",
        "wind_trans24_mean_3d",
        "wind_trans24_mean_7d",
        "zSn_mean",
        "crit_cut_length_weak_100",
        "min_ccl_pen",
        "pen_depth",
        "sn38_weak_100"]
        

For a detailed description of the features, please refer to the references below.     

## Requirements: Please insert the URL paths of the SNOWPACK server

snowpack_data_server_url_nowcast = path_snowpack_imis
snowpack_data_server_url_forecast = path_snowpack_oshd
snowpack_swiss_metnet_url_nowcast = path_snowpack_smn


## REFERENCES: 

The description of the performance of one of the Random Forest models ('RF_tidy_1') used to predict avalanche danger levels for dry-snow conditions in Switzerland during the initial live-testing in the winter season of 2020–2021:

- Pérez-Guillén, C., Techel, F., Volpi, M., and van Herwijnen, A.: Assessing the performance and explainability of an avalanche danger forecast model, EGUsphere, https://doi.org/10.5194/egusphere-2024-2374, 2024.

The developement of the models is described in:

- Pérez-Guillén, C., Techel, F., Hendrick, M., Volpi, M., van Herwijnen, A., Olevski, T., Obozinski, G., Pérez-Cruz, F., and Schweizer, J.: Data-driven automated predictions of the avalanche danger level for dry-snow conditions in Switzerland, Nat. Hazards Earth Syst. Sci., 22, 2031–2056, https://doi.org/10.5194/nhess-22-2031-2022, 2022.

## Cite this work: 


    @Article{nhess-22-2031-2022,
    AUTHOR = {P\'erez-Guill\'en, C. and Techel, F. and Hendrick, M. and Volpi, M. and van Herwijnen, A. and Olevski, T. and Obozinski, G. and P\'erez-Cruz, F. and Schweizer, J.},
    TITLE = {Data-driven automated predictions of the avalanche danger level for dry-snow conditions in Switzerland},
    JOURNAL = {Natural Hazards and Earth System Sciences},
    VOLUME = {22},
    YEAR = {2022},
    NUMBER = {6},
    PAGES = {2031--2056},
    URL = {https://nhess.copernicus.org/articles/22/2031/2022/},
    DOI = {10.5194/nhess-22-2031-2022}
    }
    
    @Article{egusphere-2024-2374,
    AUTHOR = {P\'erez-Guill\'en, C. and Techel, F. and Volpi, M. and van Herwijnen, A.},
    TITLE = {Assessing the performance and explainability of an avalanche danger forecast model},
    JOURNAL = {EGUsphere},
    VOLUME = {2024},
    YEAR = {2024},
    PAGES = {1--29},
    URL = {https://egusphere.copernicus.org/preprints/2024/egusphere-2024-2374/},
    DOI = {10.5194/egusphere-2024-2374}
    }



## Contact

Cristina Pérez Guillén: cristina.perez@slf.ch