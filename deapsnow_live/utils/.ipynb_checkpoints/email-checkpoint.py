import yagmail
import os
from pathlib import Path


def send_email(data, email_recipients, run_time):
    date_fmt_subject = "%d/%m/%Y %H:%M"
    date_fmt_attachment = "%Y-%m-%d_%H"
    fout = Path(
        f"/home/deapsnow/deapsnow/live-data/predictions/summaries/"
        f"deapsnow_results_{run_time.strftime(date_fmt_attachment)}.csv"
    )
    data.to_csv(fout, index=False)
    with yagmail.SMTP(
        os.environ["DEAPSNOW_EMAIL_ADDRESS"], os.environ["DEAPSNOW_EMAIL_PWD"]
    ) as yag:
        contents = ["The latest deapsnow results are attached."]
        yag.send(
            email_recipients,
            f"Predictions from Deapsnow Models for {run_time.strftime(date_fmt_subject)}",
            contents,
            attachments=[fout],
        )
