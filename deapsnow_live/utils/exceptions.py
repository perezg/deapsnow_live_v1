import logging


def log_exception(etype, value, tb):
    logging.exception("An exception occured!", exc_info=(etype, value, tb))
