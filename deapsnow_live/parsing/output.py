import torch
import numpy as np


def generate_output_df(preds, metadata, probs, start_date, end_date):
    output = metadata.copy()
    output.loc[:, "DL_prediction"] = preds.numpy() + 1
    output = output.rename(columns={"name": "station"})
    output.loc[:, "validto"] = end_date
    output.loc[:, "validfrom"] = start_date
    output.loc[:, ["P_level_1", "P_level_2", "P_level_3", "P_level_4"]] = probs
    output = output.reindex(
        [
            "station",
            "validfrom",
            "validto",
            "DL_prediction",
            "P_level_1",
            "P_level_2",
            "P_level_3",
            "P_level_4",
        ],
        axis=1,
    )
    return output


def generate_mt_output_df(
    pred_infer_cl,
    # pred_discrete_or,
    pred_infer_el,
    prob_out_cl,
    pred_infer_or,
    metadata,
    start_date,
    end_date,
):

    orient_enc = {
        "N": 0,
        "NE": 1,
        "E": 2,
        "SE": 3,
        "S": 4,
        "SW": 5,
        "W": 6,
        "NW": 7,
    }

    output = metadata.copy()
    output.loc[:, "DL_prediction"] = pred_infer_cl.numpy() + 1
    output.loc[:, "elevationFrom"] = pred_infer_el.numpy()

    output = output.rename(columns={"name": "station"})
    output.loc[:, "validto"] = end_date
    output.loc[:, "validfrom"] = start_date

    prob_out_cl = torch.cat(prob_out_cl).numpy()
    output.loc[:, ["P_level_1", "P_level_2", "P_level_3", "P_level_4"]] = np.asarray(
        prob_out_cl
    )
    output.loc[
        :, ["P_N", "P_E", "P_S", "P_W"]
    ] = pred_infer_or
    output = output.reindex(
        [
            "station",
            "validfrom",
            "validto",
            "DL_prediction",
            "elevationFrom",
            "P_level_1",
            "P_level_2",
            "P_level_3",
            "P_level_4",
            "P_N",
            "P_E",
            "P_S",
            "P_W",
        ],
        axis=1,
    )
    return output


def generate_mt_output_df_4dirs(
    pred_infer_cl,
    # pred_discrete_or,
    pred_infer_el,
    prob_out_cl,
    pred_infer_or,
    metadata,
    start_date,
    end_date,
):

    orient_enc = {
        "N": 0,
        "NE": 1,
        "E": 1,
        "SE": 2,
        "S": 2,
        "SW": 2,
        "W": 3,
        "NW": 3,
    }

    output = metadata.copy()
    output.loc[:, "DL_prediction"] = pred_infer_cl.numpy() + 1
    output.loc[:, "ele_prediction"] = pred_infer_el.numpy()

    output = output.rename(columns={"name": "station"})
    output.loc[:, "validto"] = end_date
    output.loc[:, "validfrom"] = start_date

    # try:
    #     prob_out_cl = torch.cat(prob_out_cl).numpy()
    # except TypeError:
    prob_out_cl = prob_out_cl.numpy()

    output.loc[:, ["P_level_1", "P_level_2", "P_level_3", "P_level_4"]] = np.asarray(
        prob_out_cl
    )
    output.loc[
        :,
        [
            f"P_O{i}" for i in np.unique(list(orient_enc.values()))
        ],  # , "P_S", "P_SW", "P_W", "P_NW"]
    ] = pred_infer_or[:, 0 : len(np.unique(list(orient_enc.values())))]

    output.loc[
        :,
        [
            f"GT_O{i}" for i in np.unique(list(orient_enc.values()))
        ],  # , "P_S", "P_SW", "P_W", "P_NW"]
    ] = pred_infer_or[:, len(np.unique(list(orient_enc.values()))) :]

    return output


def generate_mt_output_df_8dirs(
    pred_infer_cl,
    # pred_discrete_or,
    pred_infer_el,
    prob_out_cl,
    pred_infer_or,
    metadata,
    seq_duration,
):

    orient_enc = {
        "N": 0,
        "NE": 1,
        "E": 2,
        "SE": 3,
        "S": 4,
        "SW": 5,
        "W": 6,
        "NW": 7,
    }

    output = metadata.copy()
    output.loc[:, "DL_prediction"] = pred_infer_cl.numpy() + 1
    output.loc[:, "ele_prediction"] = pred_infer_el.numpy()

    output = output.rename(columns={"name": "station"})
    output.loc[:, "validto"] = output["timestamp"] + seq_duration
    output.loc[:, "validfrom"] = output["timestamp"]

    # try:
    #     prob_out_cl = torch.cat(prob_out_cl).numpy()
    # except TypeError:
    prob_out_cl = prob_out_cl.numpy()

    output.loc[:, ["P_level_1", "P_level_2", "P_level_3", "P_level_4"]] = np.asarray(
        prob_out_cl
    )
    output.loc[
        :,
        [
            f"P_O{i}" for i in np.unique(list(orient_enc.values()))
        ],  # , "P_S", "P_SW", "P_W", "P_NW"]
    ] = pred_infer_or[:, 0 : len(np.unique(list(orient_enc.values())))]

    output.loc[
        :,
        [
            f"GT_O{i}" for i in np.unique(list(orient_enc.values()))
        ],  # , "P_S", "P_SW", "P_W", "P_NW"]
    ] = pred_infer_or

    return output
