import torch
import pandas as pd
import numpy as np


def encode_metadata_tensor(data):
    """Convert a pandas dataframe into a pytorch tensor of integers."""
    output = []
    for col in data.columns:
        if data[col].dtype == object:
            coloutput = data[col].apply(lambda x: list(x.encode("utf-8")))
            coloutput = np.stack(coloutput.values)
            output.append(torch.Tensor(coloutput))
        elif np.issubdtype(data[col].dtype, np.datetime64):
            coloutput = pd.concat(
                [
                    data[col].dt.year,
                    data[col].dt.month,
                    data[col].dt.day,
                    data[col].dt.hour,
                ],
                axis=1,
            )
            output.append(torch.Tensor(coloutput.values))
        else:
            raise NotImplementedError(
                f"Data type {data[col].dtype} cannot be converted."
            )
    return torch.cat(output, dim=1).short()


def decode_metadata_tensor(data, dtypes, names, lengths):
    """Convert a tensor of ints into a pandas dataframe"""
    output = {}
    start = 0
    for icol, col in enumerate(names):
        dtype = dtypes[icol]
        length = lengths[icol]
        coldata = data[:, start: start + length]
        if dtype == object:
            output[col] = map(lambda x: bytes(x.tolist()).decode("utf-8"), coldata)
        elif np.issubdtype(dtype, np.datetime64):
            output[col] = map(lambda x: pd.Timestamp(*x.tolist()), coldata)
        else:
            raise NotImplementedError(f"Data type {dtype} cannot be converted.")
        start += length
    return pd.DataFrame(output)
