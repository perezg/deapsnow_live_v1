import torch
import numpy as np


def decode_cum_onehot(y, t=0.5, type_app="ordinal"):

    if type_app == "ordinal":
        one_hot_d = (y > t).float()
        find_first_0 = np.where(one_hot_d == 0)

        y_decode = [
            np.min(find_first_0[1][find_first_0[0] == i])
            if i in find_first_0[0]
            else y.shape[1]
            for i in range(one_hot_d.shape[0])
        ]

        y_decode = torch.LongTensor(y_decode) - 1

        return torch.max(y_decode, torch.zeros_like(y_decode))

    if type_app == "attribute":
        return torch.argmax(y, axis=1)


def convert_cum_onehot(y, n_class, type_app="ordinal", device="cpu"):

    if type_app == "ordinal":
        y = y.to(device).long()
        y_ = y + 1
        n_class += 1
        y_onehot = torch.zeros(y.shape[0], n_class, device=device)
        y_onehot.scatter_(1, y_, 1)
        return 1 - y_onehot.cumsum(1)[:, :-1]

    elif type_app == "attribute":
        y = y.to(device).long()
        y_onehot = torch.zeros(y.shape[0], n_class, device=device)
        return y_onehot.scatter_(1, y, 1.0)
