from subprocess import Popen
from pathlib import Path
import logging

from deapsnow_live.config import (
    snowpack_data_server_url_forecast,
    snowpack_data_server_url_nowcast,
    snowpack_swiss_metnet_url_nowcast,
    current_season_snowpack_data_dir_forecast,
    current_season_snowpack_data_dir_nowcast,
)


def run_wget(url, download_loc, forecast=False):
    if forecast:
        p = Popen(
            [
                "wget",
                "--mirror",
                "-np",
                "-t",
                "2",
                "-A",
                ".smet",
                "-A",
                ".pro",
                "-R",
                ".ext.smet",
                "-R",
                ".ext.pro",
                url,
                "-P",
                download_loc,
                "--cut-dirs=3",
                "-nH",
            ]
        )
    else:
        p = Popen(
            [
                "wget",
                "--mirror",
                "-np",
                "-t",
                "2",
                "-A",
                ".ext.smet",
                "-A",
                ".ext.pro",
                url,
                "-P",
                download_loc,
                "--cut-dirs=3",
                "-nH",
            ]
        )
    p.wait()


def download_nowcast(download_loc):
    run_wget(snowpack_data_server_url_nowcast, download_loc, forecast=False)
    run_wget(snowpack_swiss_metnet_url_nowcast, download_loc, forecast=False)


def download_forecast(download_loc):
    run_wget(snowpack_data_server_url_forecast, download_loc, forecast=True)


if __name__ == "__main__":
    p = Path(current_season_snowpack_data_dir_nowcast)
    if not p.exists():
        logging.warning(
            f"Location {p} for downloading snowpack data does not exist "
            "- creating it..."
        )
    download_nowcast(current_season_snowpack_data_dir_nowcast)
    p = Path(current_season_snowpack_data_dir_forecast)
    if not p.exists():
        logging.warning(
            f"Location {p} for downloading snowpack data does not exist "
            "- creating it..."
        )
    download_forecast(current_season_snowpack_data_dir_forecast)
