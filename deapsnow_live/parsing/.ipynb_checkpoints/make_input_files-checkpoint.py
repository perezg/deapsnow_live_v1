import pandas as pd
from pathlib import Path
import argparse
import logging

from deapsnow_live import config
from deapsnow_live.models.rf_models.scripts.input_data_nowcast import (
    extract_input_nowcast,
    extract_input_nowcast_aspect,
)
from deapsnow_live.models.rf_models.scripts.input_data_forecast import (
    extract_input_forecast,
    extract_input_forecast_aspect,
)
from deapsnow_live.postprocessing.danger_level import add_sector_id, add_danger_level
from deapsnow_live.models.rf_models.config.shared import path_lib
from deapsnow_live.models.rf_models.config.features import set1_smet
#from deapsnow_live.models.plain_cnn_1D.utils import merge_input_files
#from deapsnow_live.models.plain_cnn_1D.config.shared import path_lib as cnn_path_lib


def main():
    parser = argparse.ArgumentParser(
        description="Make input files from .smet and .pro data."
    )
    parser.add_argument(
        "-d",
        "--date",
        help="The date and time when the model ran rounded down to hours, "
        "the hours should be a multiple of 3 and parsable by pandas.",
        type=pd.Timestamp,
        required=False,
        default=pd.Timestamp.now().floor("H"),
    )
    parser.add_argument(
        "-n",
        "--nowcast-input",
        help="The location of the nowcast smet and pro files.",
        required=False,
        default=path_lib["data_nowcast"],
    )
    parser.add_argument(
        "-f",
        "--forecast-input",
        help="The location of the forecast smet and pro files.",
        required=False,
        default=path_lib["data_forecast"],
    )
    parser.add_argument(
        "--nowcast-output",
        help="The location of the processed nowcast model-ready nowcast files.",
        required=False,
        default=path_lib["input_nowcast"],
    )
    parser.add_argument(
        "--forecast-output",
        help="The location of the processed forecast model-ready nowcast files.",
        required=False,
        default=path_lib["input_forecast"],
        type=str,
    )
    parser.add_argument(
        "--danger-level-loc",
        help="The location of the processed forecast model-ready nowcast files.",
        required=False,
        default=config.current_season_official_danger_rating,
        type=str,
    )
    parser.add_argument(
        "--add-danger-level",
        help="Join danger level data to the input.",
        required=False,
        action="store_true",
    )

    args = parser.parse_args()
    aspects = ["north", "east", "south", "west"]
    runtime_str = args.date.strftime(config.date_time_format)
    warnregion_floc = "data/warnregions"

    idata = extract_input_nowcast(
        args.nowcast_input, args.nowcast_output, runtime_str, "no"
    )
    print("Running with:", args.nowcast_input, args.nowcast_output, runtime_str, "no")
    if args.add_danger_level:
        idata = add_sector_id(idata, warnregion_floc)
        idata = add_danger_level(idata, args.danger_level_loc)
    p = Path(args.nowcast_output)
    if not p.exists():
        logging.warning(
            f"Model-ready input file location {args.nowcast_output} "
            "does not exist - creating it now..."
        )
        p.mkdir(parents=True, exist_ok=True)
    fout = (
        args.nowcast_output
        + "/"
        + runtime_str[0:10]
        + "_"
        + runtime_str[11:13]
        + ".csv"
    )
    print(
        f"Saving {idata[set1_smet[0]].dropna().shape[0]} non null "
        f"lines for input data for nowcast at {fout}"
    )
    idata.to_csv(fout)

    idata = extract_input_forecast(
        args.forecast_input, args.nowcast_input, args.forecast_output, runtime_str, "no"
    )
    if args.add_danger_level:
        idata = add_sector_id(idata, warnregion_floc)
        idata = add_danger_level(idata, args.danger_level_loc)
    p = Path(args.forecast_output)
    if not p.exists():
        logging.warning(
            f"Model-ready input file location {args.forecast_output} "
            "does not exist - creating it now..."
        )
        p.mkdir(parents=True, exist_ok=True)
    fout = (
        args.forecast_output
        + "/"
        + runtime_str[0:10]
        + "_"
        + runtime_str[11:13]
        + ".csv"
    )
    print(
        f"Saving {idata[set1_smet[0]].dropna().shape[0]} non null "
        f"lines for input data for forecast at {fout}"
    )
    idata.to_csv(fout)

    for aspect in aspects:
        # Collect nowcast by aspect
        idata = extract_input_nowcast_aspect(
            args.nowcast_input, args.nowcast_output, runtime_str, aspect, "no"
        )
        if args.add_danger_level:
            idata = add_sector_id(idata, warnregion_floc)
            idata = add_danger_level(idata, args.danger_level_loc)
        p = Path(args.nowcast_output)
        if not p.exists():
            logging.warning(
                f"Model-ready input file location {args.nowcast_output} "
                "does not exist - creating it now..."
            )
            p.mkdir(parents=True, exist_ok=True)
        fout = (
            args.nowcast_output
            + "/"
            + runtime_str[0:10]
            + "_"
            + runtime_str[11:13]
            + "_"
            + aspect
            + ".csv"
        )
        print(
            f"Saving {idata[set1_smet[0]].dropna().shape[0]} non null "
            f"lines for input data for nowcast, aspect {aspect} at {fout}"
        )
        idata.to_csv(fout)
        # Collect forecast by aspect
        idata = extract_input_forecast_aspect(
            args.forecast_input, args.nowcast_input, args.forecast_output, runtime_str, aspect, "no"
        )
        if args.add_danger_level:
            idata = add_sector_id(idata, warnregion_floc)
            idata = add_danger_level(idata, args.danger_level_loc)
        p = Path(args.forecast_output)
        if not p.exists():
            logging.warning(
                f"Model-ready input file location {args.nowcast_output} "
                "does not exist - creating it now..."
            )
            p.mkdir(parents=True, exist_ok=True)
        fout = (
            args.forecast_output
            + "/"
            + runtime_str[0:10]
            + "_"
            + runtime_str[11:13]
            + "_"
            + aspect
            + ".csv"
        )
        print(
            f"Saving {idata[set1_smet[0]].dropna().shape[0]} non null "
            f"lines for input data for forecast, aspect {aspect} at {fout}"
        )
        idata.to_csv(fout)

    
    


if __name__ == "__main__":
    main()
