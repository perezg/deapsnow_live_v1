from deapsnow_live.postprocessing.perf_eval import merge_preds
import pandas as pd
import numpy as np

def test_merge_preds(tmpdir):
    # prep train data to look like nowcast prediction
    prediction_summary_f = tmpdir.join("deapsnow_results_2020-01-01_15.csv")
    # adjust training data as if it was a nowcast that ran at 15:00
    # training data is every day at 17:00
    train_data = pd.read_csv(
        "data/data_all/Dry_flat_all_mean.csv",
        parse_dates=["validFromDate", "validToDate"]
    )
    train_data.loc[:, "validFromDate"] += pd.Timedelta(hours=1)
    train_data.loc[:, "validToDate"] -= pd.Timedelta(hours=2)
    train_data = train_data.rename({
        "validFromDate": "validfrom",
        "validToDate": "validto",
        "station_code": "station",
        "dangerLevel": "DL_prediction",
    }, axis=1)
    train_data.loc[:, "model_type"] = "test"
    train_data.loc[:, "prediction_type"] = "nowcast"
    train_data.loc[:, "rf_type"] = np.nan
    train_data.loc[:, "orientation"] = "f"
    train_data.loc[train_data["DL_prediction"] == 5, "DL_prediction"] -= 1
    train_data = train_data.loc[:, [
        "validfrom", "validto", "station", "DL_prediction",
        "model_type", "prediction_type", "rf_type", "orientation",
    ]].copy()
    train_data = train_data.loc[train_data["validfrom"].dt.year == 2020, :].copy()
    train_data.to_csv(prediction_summary_f, index=False)
    # prep danger level data to have proper column names
    tmpdir.mkdir("danger")
    danger_f = tmpdir.join("danger", "dangerrating_2.csv")
    danger = pd.read_csv("data/train_data/dangerrating_2.csv").drop(["Unnamed: 0"], axis=1)
    danger.loc[:, "indicator"] = "ABOVE"
    danger = danger.rename({
        "validFromDate": "valid_from",
        "validToDate": "valid_to",
        "altitudeLow": "altitude",
        "sectorId": "sector_id",
        "aspectFrom": "from",
        "aspectEnd": "to",
        "dangerLevel": "level_numeric",

    }, axis=1)
    danger.loc[:, "level_detail_numeric"] = danger["level_numeric"] * 1.0
    danger.loc[:, "drySnow"] = ((danger["conditions"] == "dry") * 1).astype(int)
    danger.to_csv(danger_f)
    data = merge_preds(
        pd.Timestamp(2020, 1, 1),
        pd.Timestamp(2020, 2, 1),
        tmpdir,
        danger_f,
        "data/warnregions/warnreg_21.csv",
        "nowcast",
    )
    assert (data["DL_prediction"] == data["level_numeric"]).all()
