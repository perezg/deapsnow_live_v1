import argparse
from datetime import datetime
import pandas as pd
import logging
import numpy as np
from pathlib import Path

from deapsnow_live.postprocessing.danger_level import get_dl_average
from deapsnow_live.config import date_time_format
from deapsnow_live.models.rf_models.config.shared import path_lib as rf_models_path_lib
from deapsnow_live.models.plain_cnn_1D.config.shared import path_lib as cnn_model_path_lib
from deapsnow_live.config import preictions_summary_location


def convert_output_aspects(data):
    output = []
    for orientation in ["f", "n", "e", "s", "w"]:
        col_xref = {
            f"DL_{orientation}": "DL_prediction",
            f"P1_{orientation}": "P_level_1",
            f"P2_{orientation}": "P_level_2",
            f"P3_{orientation}": "P_level_3",
            f"P4_{orientation}": "P_level_4",
        }
        orient_data = (
            data.loc[
                :,
                [
                    "station",
                    "validfrom",
                    "validto",
                    f"DL_{orientation}",
                    f"P1_{orientation}",
                    f"P2_{orientation}",
                    f"P3_{orientation}",
                    f"P4_{orientation}",
                ],
            ]
            .copy()
            .rename(col_xref, axis=1)
        )
        orient_data.loc[:, "orientation"] = orientation
        output.append(orient_data)
    output = pd.concat(output, axis=0, ignore_index=True)
    return output


def combine_output(flocs, run_time):
    fout_date_fmt = "%Y-%m-%d_%H"
    print(f"Starting predictions summary for runtime {run_time}")
    output = []
    for floc in flocs:
        for fin in floc.glob(f"*{run_time.strftime(fout_date_fmt)}*csv"):
            print(f"Reading model predictions at {fin}")
            data = pd.read_csv(fin)
            data = data.drop(["Unnamed: 0"], axis=1)
            if "type1" in fin.name and "flat" in fin.name:
                continue  # this is already part of type1 all aspects
            if "aspects" in fin.name:
                data = convert_output_aspects(data)
            else:
                data.loc[:, "orientation"] = "f"
            data.loc[:, "prediction_type"] = fin.parent.stem
            data.loc[:, "model_type"] = fin.parents[1].stem
            if fin.parents[1].stem == "rf_models":
                data.loc[:, "rf_type"] = "_".join(fin.stem.split("_")[-3:])
            else:
                data.loc[:, "rf_type"] = np.nan
            data = data.loc[data["DL_prediction"].notnull(), :].copy()
            print(f"Adding {data.shape[0]} lines to summary from {fin}")
            output.append(data)
    if len(output) == 0:
        return None
    output = pd.concat(output, axis=0, ignore_index=True)
    output.loc[:, "DL_average"] = get_dl_average(output)
    return output


if __name__ == "__main__":
    flocs = [
        Path(cnn_model_path_lib["prediction_output_nowcast"]),
        Path(cnn_model_path_lib["prediction_output_forecast"]),
        Path(rf_models_path_lib["output_nowcast"]),
        Path(rf_models_path_lib["output_forecast"]),
    ]
    parser = argparse.ArgumentParser(
        description="Combine the output from all models in one file."
    )
    parser.add_argument(
        "--date",
        "-d",
        type=lambda x: datetime.strptime(x, date_time_format),
        help="It is validto (last date) for nowcast and "
        "validfrom (first date) for forecast - also corresponds to the time"
        "when the script is running.",
        required=True,
    )
    args = parser.parse_args()
    data = combine_output(flocs, args.date)
    fout_date_time_format = "%Y-%m-%d_%H"
    p = Path(preictions_summary_location)
    if not p.exists():
        logging.warning(
            f"Location for model prediction summmary {preictions_summary_location} "
            "does not exst - creating it now..."
        )
        p.mkdir(parents=True, exist_ok=True)
    data.to_csv(
        p / f"deapsnow_results_{args.date.strftime(fout_date_time_format)}.csv"
    )
