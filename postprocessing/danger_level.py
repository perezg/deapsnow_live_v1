from pathlib import Path
import pandas as pd
import numpy as np

def get_dl_average(data):
    return (
        data["P_level_1"] * 1.0
        + data["P_level_2"] * 2.0
        + data["P_level_3"] * 3.0
        + data["P_level_4"] * 4.0
    )

