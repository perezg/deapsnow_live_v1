.PHONY: all prep train input_files predict
RUNTIME ?= $(shell date +\%Y-\%m-\%d\ \%H:00:00)

all:
	@echo "Please run either 'make train' or 'make predict'"

prep:
	@echo "\n\n--------------------RUNNING PREP---------------------\n\n"
	@python -m deapsnow_live.models.plain_cnn_1D.run_prep

train:
	@echo "\n\n--------------------STARTED TRAINING MODELS---------------------\n\n"
	@echo "-------------------STARTED TRAINING RF MODELS-------------------"
	@python -m deapsnow_live.models.rf_models.run_training
	@echo "------------------FINISHED TRAINING RF MODELS-------------------"
	@echo "------------------STARTED TRAINING CNN MODELS-------------------"
	@python -m deapsnow_live.models.plain_cnn_1D.run_training
	@echo "-----------------FINISHED TRAINING CNN MODELS-------------------"

download_snowpack_data:
	@echo "\n\n--------------------DOWNLOADING LATEST SNOWPACK DATA---------------------\n\n"
	@python -m deapsnow_live.parsing.download_data

input_files:
	@echo "\n\n--------------------MAKING INPUT FILES FOR $(RUNTIME)---------------------\n\n"
	@python -m deapsnow_live.parsing.make_input_files -d "$(RUNTIME)"

predict:
	@echo "\n\n-----------------------STARTED PREDICTION FOR $(RUNTIME)-----------------------\n\n"
	@echo "-----------------------RUNNING RF MODELS------------------------"
	@python -m deapsnow_live.models.rf_models.run_inference_flat -d "$(RUNTIME)" --forecast
	@python -m deapsnow_live.models.rf_models.run_inference_aspects -d "$(RUNTIME)" --forecast
	@echo "-----------------------RUNNING CNN MODELS------------------------"
	@python -m deapsnow_live.models.plain_cnn_1D.run_inference -d "$(RUNTIME)" --forecast
	@echo "\n\n-----------------------MAKING OUTPUT SUMMARY FOR $(RUNTIME)-----------------------\n\n"
	@python -m deapsnow_live.postprocessing.output -d "$(RUNTIME)"

predict_wet:
	@echo "\n\n-----------------------STARTED PREDICTION FOR $(RUNTIME) (WET MODELS)-----------------------\n\n"
	@python -m deapsnow_live.models.wet_snow_aai.run_inference -d "$(RUNTIME)"

evaluate:
	@echo "\n\n-----------------------STARTED EVALUATION FOR $(TSTART) - $(TEND)-----------------------\n\n"
	@python -m deapsnow_live.postprocessing.run_perf_eval -s "$(TSTART)" -e "$(TEND)" -g prediction_type model_type rf_type orientation -p nowcast -o summary.csv